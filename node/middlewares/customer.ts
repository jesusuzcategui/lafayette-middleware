import {json} from 'co-body'
import axios from 'axios'
import { ENTITY_ID } from '../utils/constants'

export async function customer(ctx: Context, next: () => Promise<any>) {
  const body : CustomerRequest = await json(ctx.req);

  const http=axios.create({
    headers:{
      VtexIdclientAutCookie: ctx.vtex.authToken,
      "Cache-Control":"no-cache",
      "X-Vtex-Use-Https":"true"
    }
  });

  const request = {
    "firstName": body.firstName,
    "lastName": body.lastName,
    "corporateName": body.corporateName,
    "corporateDocument": body.nitCompany,
    "phone": body.phone,
    "terms": body.terms,
    "email": body.email,
    "preferencia":body.preferencia
  }  
  var response = {
    status:201,
    message:"",
    id:""
  }
  try {
    const { data, status } = await http.post(`http://${ctx.vtex.account}.myvtex.com/api/dataentities/${ENTITY_ID}/documents`,request);
    if(status==201){
      response.message="Customer created";
      response.id=data.DocumentId;        
    }else{
      response.status=status;
      response.message="Client creation failed";
    }
  } catch (error) {
    console.log(error);
    response.status=500;
    response.message="Error unexpected ";
  }
  ctx.status = response.status;
  ctx.body = response; 
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Methods', 'POST, OPTIONS');
  await next()
}