import { json } from 'co-body';
import axios from 'axios'
import { APPKEY, APPLICATION_TOKEN, ENTITY_ID } from '../utils/constants'

export async function rememberEmail(ctx: Context, next: () => Promise<any>) {
  const body: ListCustomerRequest = await json(ctx.req);

  let tipoDocumento = (body.typeDoc == "Cedula") ? `(documentType = Cedula) OR (documentType = cedulaCOL)` : `documentType = ${body.typeDoc}`;
  
  let urlQuery = `http://${ctx.vtex.account}.myvtex.com/api/dataentities/${ENTITY_ID}/search?_fields=document,phone,email,approved&_where=((document=${body.numberDoc}) AND (${tipoDocumento})) OR phone=${body.numberPhone}`;
  console.log("Url Query", urlQuery);

  const requestConfig: any = {
    method: "GET",
    url: urlQuery,
    headers: {
      VtexIdclientAutCookie: ctx.vtex.authToken,
      "X-VTEX-API-AppToken": APPLICATION_TOKEN,
      "X-VTEX-API-AppKey": APPKEY
    }
  };

  let response: any = {
    status: 200,
    data: null
  };

  try {
    const { data, status } = await axios(requestConfig);

    if (data.length > 0) {
      response.data = data;
      response.status = status;
    } else {
      response.data = null;
      response.status = 404;
    }
  } catch (error) {
    response.data = null;
    response.status = 500;
    console.log("Error", error);
  }

  ctx.status = response.status;
  ctx.body = response;

  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Methods', 'POST, OPTIONS');
  await next();
}