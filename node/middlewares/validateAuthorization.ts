//import { ForbiddenError } from '@vtex/api'

import { APPLICATION_TOKEN as AUTHORIZATION_CODE, APPKEY } from '../utils/constants'

export async function validateCheckoutAuthorization(
  ctx: Context,
  next: () => Promise<any>
) {
  /*const {
    headers: { authorization },
  } = ctx

  if (!authorization || authorization !== AUTHORIZATION_CODE) {
    throw new ForbiddenError('Authorization token does not match')
  }*/
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('X-VTEX-API-AppToken', AUTHORIZATION_CODE);
  ctx.set('X-VTEX-API-AppKey', APPKEY);
  ctx.set('Access-Control-Allow-Methods', 'POST, OPTIONS, GET');
  await next()
}
