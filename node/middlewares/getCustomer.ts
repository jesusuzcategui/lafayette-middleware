import {json} from 'co-body'
import axios from 'axios'
import { ENTITY_ID } from '../utils/constants'

export async function getCustomer(ctx: Context, next: () => Promise<any>) {
  const body : ListCustomerRequest = await json(ctx.req);

  const http=axios.create({
    headers:{
      VtexIdclientAutCookie: ctx.vtex.authToken,
      "Cache-Control":"no-cache",
      "X-Vtex-Use-Https":"true",
      "REST-Range":"resources="+body.pageStart+"-"+body.pageEnd
    }
  });

  
  var response = {
    status:201,
    message:"",
    data:""
  }
  try {
    const { data, status } = await http.get(`http://${ctx.vtex.account}.myvtex.com/api/dataentities/${ENTITY_ID}/search?_fields=email,approved,corporateName`);
    if(status==200 && data.length>0){
      response.data=data;        
    }else{
      response.status=404;
      response.message="Data not found";
    }
  } catch (error) {
    response.status=500;
    response.message="Error unexpected ";
  }
  
  ctx.status = response.status;
  ctx.body = response; 
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Methods', 'POST, OPTIONS');
  await next()
}