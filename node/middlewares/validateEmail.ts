import { json } from 'co-body';
import axios from 'axios'
import { APPKEY, APPLICATION_TOKEN, ENTITY_ID } from '../utils/constants'

export async function validateEmail(ctx: Context, next: () => Promise<any>) {
  const body: ListCustomerRequest = await json(ctx.req);

  let urlQuery = `http://${ctx.vtex.account}.myvtex.com/api/dataentities/${ENTITY_ID}/search?_fields=email,approved&_where=email=${body.email.toLowerCase()}`;
  console.log("Url Query", urlQuery);

  const requestConfig: any = {
    method: "GET",
    url: urlQuery,
    headers: {
      VtexIdclientAutCookie: ctx.vtex.authToken,
      "X-VTEX-API-AppToken": APPLICATION_TOKEN,
      "X-VTEX-API-AppKey": APPKEY
    }
  };

  let response: any = {
    status: 200,
    data: null
  };

  try {
    const { data, status } = await axios(requestConfig);

    if (data.length > 0) {
      response.data = data;
      response.status = status;
    } else {
      response.data = null;
      response.status = 404;
    }
  } catch (error) {
    response.data = null;
    response.status = 500;
    console.log("Error", error);
  }

  ctx.status = response.status;
  ctx.body = response;

  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Methods', 'POST, OPTIONS');
  await next();
}