interface CustomerRequest{
    corporateName: string,
    nitCompany: string,
    firstName: string,
    lastName: string,
    phone: string,
    terms: boolean,
    preferencia:String,
    email: string
}