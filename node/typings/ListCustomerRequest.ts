interface ListCustomerRequest{
    pageStart: number,
    pageEnd: number,
    numberPhone: string,
    numberDoc: string,
    typeDoc: string,
    email: string
}